﻿using receipt_scanner.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace receipt_scanner.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}