﻿using receipt_scanner.ViewModels;
using receipt_scanner.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace receipt_scanner
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
        }

    }
}
